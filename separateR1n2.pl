$|++;
use strict;
use Data::Dumper;

#my $file = 'M0146.6179.1.38891.fastq';
my $file = $ARGV[0];

my $read1file = $file;
$read1file =~ s/.fastq$/.read1.fastq/;
my $read2file = $file;
$read2file =~ s/.fastq$/.read2.fastq/;

open (INFILE, $file) || die "Fail to open $file\n$!\n";
open (READ1, ">$read1file") || die "Fail to open $read1file\n$!\n";
open (READ2, ">$read2file") || die "Fail to open $read2file\n$!\n";
my $filteredFile = $file;
$filteredFile =~ s/.fastq$/.R1filtered.fastq/;
open (FILTERED1, ">$filteredFile") || die "Fail to open $filteredFile\n$!\n";
$filteredFile = $file;
$filteredFile =~ s/.fastq$/.R2filtered.fastq/;
open (FILTERED2, ">$filteredFile") || die "Fail to open $filteredFile\n$!\n";
$filteredFile = $file;
$filteredFile =~ s/.fastq$/.bothfiltered.fastq/;
open (FILTEREDBOTH, ">$filteredFile") || die "Fail to open $filteredFile\n$!\n";
my ($total, $notFiltered, $filtered1, $filtered2, $filteredBoth) = (0, 0, 0, 0, 0);
my ($reportBlock, $trigger) = (100000, 100000);
print "Procesisng $file..";
while (<INFILE>) {
	# read 1
	s/\r\n$|\r$|\n$//; my $read1id = $_;
	my $read1seq = <INFILE>; $read1seq =~ s/\r\n$|\r$|\n$//; 
	my $ignore = <INFILE>;
	my $read1quality = <INFILE>; $read1quality =~ s/\r\n$|\r$|\n$//; 
	# read 2
	my $read2id = <INFILE>; $read2id =~ s/\r\n$|\r$|\n$//; 
	my $read2seq = <INFILE>; $read2seq =~ s/\r\n$|\r$|\n$//; 
	$ignore = <INFILE>;
	my $read2quality = <INFILE>; $read2quality =~ s/\r\n$|\r$|\n$//; 
	
	if (0!=isFiltered($read1id)) {
		if (0!=isFiltered($read2id)) {
			# both read1, read2 filtered
			print FILTEREDBOTH getID($read1id), "\n";
			print FILTEREDBOTH $read1seq, "\n";
			print FILTEREDBOTH '+', "\n";
			print FILTEREDBOTH $read1quality, "\n";
			print FILTEREDBOTH getID($read2id), "\n";
			print FILTEREDBOTH $read2seq, "\n";
			print FILTEREDBOTH '+', "\n";
			print FILTEREDBOTH $read2quality, "\n";
			$filteredBoth++;
		} else {
			# only read 1 is filtered
			print FILTERED1 getID($read1id), "\n";
			print FILTERED1 $read1seq, "\n";
			print FILTERED1 '+', "\n";
			print FILTERED1 $read1quality, "\n";
			print FILTERED1 getID($read2id), "\n";
			print FILTERED1 $read2seq, "\n";
			print FILTERED1 '+', "\n";
			print FILTERED1 $read2quality, "\n";
			$filtered1++;
		}
	} else {
		if (0!=isFiltered($read2id)) {
			# only read 2 is filtered
			print FILTERED2 getID($read1id), "\n";
			print FILTERED2 $read1seq, "\n";
			print FILTERED2 '+', "\n";
			print FILTERED2 $read1quality, "\n";
			print FILTERED2 getID($read2id), "\n";
			print FILTERED2 $read2seq, "\n";
			print FILTERED2 '+', "\n";
			print FILTERED2 $read2quality, "\n";
			$filtered2++;
		} else {
			# both read1 and read2 stay
			print READ1 getID($read1id), "\n";
			print READ1 $read1seq, "\n";
			print READ1 '+', "\n";
			print READ1 $read1quality, "\n";
			print READ2 getID($read2id), "\n";
			print READ2 $read2seq, "\n";
			print READ2 '+', "\n";
			print READ2 $read2quality, "\n";
			$notFiltered++;
		}
	}
	
	$total++;
	if ($total>=$trigger) {
		printf " %.0fK..", $total/1000;
		$trigger+=$reportBlock;
	}
}
close INFILE;
close READ1;
close READ2;
print " done!\n";

printf "Total reads   : %d pairs\n", $total;
printf "Passed filter : %d pairs (%.2f)\n", $notFiltered, $notFiltered*100/$total;
printf "R1 filtered   : %d pairs (%.2f)\n", $filtered1, $filtered1*100/$total;
printf "R2 filtered   : %d pairs (%.2f)\n", $filtered2, $filtered2*100/$total;
printf "Both filter   : %d pairs (%.2f)\n", $filteredBoth, $filteredBoth*100/$total;

exit 0;

sub isFiltered {
	my ($_) = @_;
	my @parts = split(/\s+/, $_);
	if (scalar(@parts)==2) {
		my @bits = split(/\:/, $parts[1]);
		if (scalar(@bits)>=2) {
			if ('Y' eq $bits[1]) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	} else {
		return 0;
	}
}

sub getID {
	my ($_) = @_;
	my @parts = split(/\s+/, $_);
	if (scalar(@parts)==2) {
		my @bits = split(/\:/, $parts[1]);
		if (scalar(@bits)>=2) {
			return $parts[0].'/'.$bits[0];
		} else {
			return $_;
		}
	} else {
		return $_;
	}
}

