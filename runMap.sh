RLIB=$1 ; \
RUN=$2 ; \
FILEPREFIX=${RLIB}-${RUN} ; \
LOGFILE=${FILEPREFIX}.map.log ; \
echo `hostname` | tee ${LOGFILE} ; \
echo MAP START `date` | tee -a ${LOGFILE} ; \
module load bowtie2
module load picard
module load samtools
bowtie2 -p 32 -t --phred33 --very-sensitive-local --un-bz2 ${FILEPREFIX}.unpaired.unaligned.sam.bz2 --al-bz2 ${FILEPREFIX}.unpaired.aligned.sam.bz2 --un-conc-bz2 ${FILEPREFIX}.discordant.sam.bz2 --al-conc-bz2 ${FILEPREFIX}.concordant.sam.bz2 PUT/PUTPvi181a -1 ${FILEPREFIX}.trimmed.fastq.1 -2 ${FILEPREFIX}.trimmed.fastq.2 -S ${FILEPREFIX}.sam 2>&1 | tee -a ${LOGFILE} ; \
echo MAP END `date` | tee -a ${LOGFILE} ; \
echo REFOMAT START `date` | tee -a ${LOGFILE} ; \
samtools view -Sbh -o ${FILEPREFIX}.bam ${FILEPREFIX}.sam ; \
samtools sort -m 4000000000 ${FILEPREFIX}.bam ${FILEPREFIX}.sorted ; \
samtools index ${FILEPREFIX}.sorted.bam ; \
samtools flagstat ${FILEPREFIX}.sorted.bam > ${FILEPREFIX}.sorted.flagstat ; \
samtools idxstats ${FILEPREFIX}.sorted.bam > ${FILEPREFIX}.sorted.idxstats ; \
echo REFOMAT END `date` | tee -a ${LOGFILE} ; \
echo DEDUPLICATE START `date` | tee -a ${LOGFILE} ; \
picard MarkDuplicates -Xmx8g MAX_FILE_HANDLES=100 AS=true I=${FILEPREFIX}.sorted.bam O=${FILEPREFIX}.unique.bam M=${FILEPREFIX}.unique.metric.txt ; \
samtools index ${FILEPREFIX}.unique.bam ; \
samtools flagstat ${FILEPREFIX}.unique.bam | tee ${FILEPREFIX}.unique.flagstat ; \
samtools idxstats ${FILEPREFIX}.unique.bam | tee ${FILEPREFIX}.unique.idxstats ; \
echo DEDUPLICATE END `date` | tee -a ${LOGFILE}
