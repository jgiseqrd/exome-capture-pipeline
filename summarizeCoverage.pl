use strict;
my $file = $ARGV[0];
open(INFILE, $file) || die "Fail to open $file\n$!\n";
my @depthCounts = ();
while (<INFILE>) {
	chomp;
	my @parts = split(/\t/);
	foreach my $depth (split(/,/, $parts[1])) {
		$depthCounts[$depth]++;
	}
}
close INFILE;

my $maxDepth = scalar(@depthCounts);
my $total = 0;
my $skipLevel=20; my $skipDepth = 5;
my @depthLevels = (); my @depthLevelCounts = ();
for(my $i=0; $i<$skipLevel; ++$i) { push @depthLevels, $i; push @depthLevelCounts, 0; } 
for(my $i=$skipLevel; $i<=100; $i+=$skipDepth) { push @depthLevels, $i; push @depthLevelCounts, 0; }
my $noOfLevels = scalar(@depthLevels);
for(my $i=0; $i<=$maxDepth; ++$i) {
	next if (!defined $depthCounts[$i]);
	$total += $depthCounts[$i];
	for(my $j=0; $j<$noOfLevels; ++$j) {
		if ($i>=$depthLevels[$j]) {
			$depthLevelCounts[$j] += $depthCounts[$i];
		}
	}
}

my $prefix = $file;
$prefix =~ s/\.unique\.gcov$//;
$prefix =~ s/\.gcov$//;
print $prefix, "\n";
print "\n";
print "Total bases:\t$total\n";
print "Depth Coverage Summary\n";
print "MinDepth\t#Bases\t%Bases\n";
for(my $j=0; $j<$noOfLevels; ++$j) {
	printf "%d\t%d\t%.2f\n", $depthLevels[$j], $depthLevelCounts[$j], $depthLevelCounts[$j]*100/$total;
}

print "\n";
print "Complete Depth Coverage\n";
print "Depth\t#Bases\t%Bases\n";
for(my $i=0; $i<=$maxDepth; ++$i) {
	next if (!defined $depthCounts[$i]);
	printf "%d\t%d\t%.2f\n", $i, $depthCounts[$i], $depthCounts[$i]*100/$total;
}

exit 0;

