RLIB=$1 ; \
RUN=$2 ; \
FILEPREFIX=${RLIB}-${RUN} ; \
DIR=${RUN:0:4} ; \
DIRPREFIX=${DIR:0:2} ; \
DIRSUFFIX=${DIR:2:2} ; \
LOGFILE=${FILEPREFIX}.process.log ; \
echo `hostname` | tee ${LOGFILE} ; \
echo `date` Retrieving ${RUN}.. | tee -a ${LOGFILE} ; \
rsync -avL --progress /global/dna/dm_archive/sdm/illumina/00/${DIRPREFIX}/${DIRSUFFIX}/${RUN}.fastq.gz ${FILEPREFIX}.fastq.gz 2>&1 | tee -a ${LOGFILE} ; \
echo `date` Gunzipping .. | tee -a ${LOGFILE} ; \
gunzip ${FILEPREFIX}.fastq.gz 2>&1 | tee -a ${LOGFILE} ; \
echo `date` Separate Read 1 and Read 2 .. | tee -a ${LOGFILE} ; \
perl separateR1n2.pl ${FILEPREFIX}.fastq 2>&1 | tee -a ${LOGFILE} ; \
echo TRIM START `date` | tee -a ${LOGFILE} ; \
cutadapt-1.0/RNASeq-quality -q 20 -m 75 -o ${FILEPREFIX}.trimmed.fastq --too-short-output=${FILEPREFIX}.tooshort --format fastq ${FILEPREFIX}.read1.fastq ${FILEPREFIX}.read2.fastq 2>&1 | tee -a ${LOGFILE} ; \
echo `date` ENDED | tee -a ${LOGFILE}
