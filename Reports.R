data <- read.table(file="computeMappingStatistics_forReports.txt",sep=",",row.names=1,header=T);
pdf(file="kae.switch.12.reports.pdf")

par(mfrow=c(2,1)) # all plots on one page 

##Genome and Transcriptome Mapping
plot(data[,6],ylim=c(0,100),type="l",lwd=2.5,col="red",xaxt="n",xlab="LibraryName",ylab="% mapping",cex.lab=0.7,cex.axis=0.4)
axis(side=1,1:84,rownames(data),las=2,cex.axis=0.4)
lines(data[,18],col="blue",lwd=2.5)
legend('bottomright',c("% Transcriptome Mapping","% Genome Mapping"),cex=0.5,col=c("red","blue"),lty=1,lwd=2.5)


##Average Read Depth and Median Read depth (Transcriptome)
plot(data[,10],ylim=c(0,70),type="l",lwd=2.5,col="red",xaxt="n",xlab="LibraryName",ylab="Read Depth",xaxt="n",cex.lab=0.7,cex.axis=0.4)
axis(side=1,1:84,rownames(data),las=2,cex.axis=0.4)
lines(data[,11],col="blue",lwd=2.5)
legend('topright',c("Median Depth","% Mean Depth"),cex=0.5,col=c("red","blue"),lty=1,lwd=2.5)

##Capture Efficiency
barplot(data[,22],ylim=c(0,100),ylab="% Capture efficiency",col="brown",xlab="Library Name",names.arg=rownames(data),las=2,cex.lab=0.5,cex.names=0.4,cex.axis=0.5)

##Fold enrichment
barplot(data[,24],ylim=c(0,10),ylab="Fold enrichment (x)",col="brown",xlab="Library Name",names.arg=rownames(data),las=2,cex.lab=0.5,cex.names=0.4,cex.axis=0.5)

#% Duplicates
barplot(data[,9],ylim=c(0,20),ylab="% Duplicates (Transcriptome)",col="brown",xlab="Library Name",names.arg=rownames(data),las=2,cex.lab=0.5,cex.names=0.4,cex.axis=0.5)




#Total Reads and Filtered Reads

barplot(data[,1]/1e6,col="lightblue",xlab="Library Name",ylab="Filtered Reads in millions",las=2,names.arg=rownames(data),cex.lab=0.5,cex.names=0.4,cex.axis=0.5)

dev.off();
