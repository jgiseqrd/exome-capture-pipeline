#!/us/bin/perl

open(RF,"Data.txt");
open (WF,">runTrim_qsub.sh");
open(WF1,">runMap_qsub.sh");
open(WF2,">runMapGenome_qsub.sh");
open(WF3,">runCoverage_qsub.sh");
open(WF4, ">runCombinedScript_qsub.sh");
while($line=<RF>)	{
	chomp($line);
	@cols=split(/\t/,$line);
	print WF "qsub -N $cols[1]s1 -pe pe_slots 1 -l ram.c=1G -cwd ./runFetchSplitTrim.sh $cols[1] $cols[0]\n";
	print WF1 "qsub -N $cols[1]s2 -pe pe_slots 16 -l ram.c=6G -cwd ./runMap.sh $cols[1] $cols[0]\n";
	print WF2 "qsub -N $cols[1]s3 -pe pe_slots 16 -l ram.c=6G -cwd ./runMapGenome.sh $cols[1] $cols[0]\n";
	print WF3 "qsub -N $cols[1]s4 -pe pe_slots 16 -l ram.c=6G -cwd ./runCoverage.sh $cols[1] $cols[0]\n";
	print WF4 "qsub -N $cols[1]a1 -pe pe_slots 16 -l ram.c=1G -l h_rt=12:00:00 -cwd ./runCombinedScript.sh $cols[1] $cols[0]\n";

}
close RF;
close WF;
close WF1;
close WF2;
close WF3;
close WF4;
