RLIB=$1 ; \
RUN=$2 ; \
FILEPREFIX=${RLIB}-${RUN} ; \
DIR=${RUN:0:4} ; \
DIRPREFIX=${DIR:0:2} ; \
DIRSUFFIX=${DIR:2:2} ; \
LOGFILE=${FILEPREFIX}.process.log ; \
echo `hostname` | tee ${LOGFILE} ; \
echo `date` Retrieving ${RUN}.. | tee -a ${LOGFILE} ; \
rsync -avL --progress /global/dna/dm_archive/sdm/illumina/00/${DIRPREFIX}/${DIRSUFFIX}/${RUN}.fastq.gz ${FILEPREFIX}.fastq.gz 2>&1 | tee -a ${LOGFILE} ; \
echo `date` Gunzipping .. | tee -a ${LOGFILE} ; \
gunzip ${FILEPREFIX}.fastq.gz 2>&1 | tee -a ${LOGFILE} ; \
echo `date` Separate Read 1 and Read 2 .. | tee -a ${LOGFILE} ; \
perl separateR1n2.pl ${FILEPREFIX}.fastq 2>&1 | tee -a ${LOGFILE} ; \
echo TRIM START `date` | tee -a ${LOGFILE} ; \
cutadapt-1.0/RNASeq-quality -q 20 -m 75 -o ${FILEPREFIX}.trimmed.fastq --too-short-output=${FILEPREFIX}.tooshort --format fastq ${FILEPREFIX}.read1.fastq ${FILEPREFIX}.read2.fastq 2>&1 | tee -a ${LOGFILE} ; \
echo `date` ENDED | tee -a ${LOGFILE}

##Run Transcriptome mapping

FILEPREFIX=${RLIB}-${RUN} ; \
LOGFILE=${FILEPREFIX}.map.log ; \
echo `hostname` | tee ${LOGFILE} ; \
echo MAP START `date` | tee -a ${LOGFILE} ; \
module load bowtie2
module load picard
module load samtools
bowtie2 -p 16 -t --phred33 --very-sensitive-local --un-bz2 ${FILEPREFIX}.unpaired.unaligned.sam.bz2 --al-bz2 ${FILEPREFIX}.unpaired.aligned.sam.bz2 --un-conc-bz2 ${FILEPREFIX}.discordant.sam.bz2 --al-conc-bz2 ${FILEPREFIX}.concordant.sam.bz2 PUT/PUTPvi181a -1 ${FILEPREFIX}.trimmed.fastq.1 -2 ${FILEPREFIX}.trimmed.fastq.2 -S ${FILEPREFIX}.sam 2>&1 | tee -a ${LOGFILE} ; \
echo MAP END `date` | tee -a ${LOGFILE} ; \
echo REFOMAT START `date` | tee -a ${LOGFILE} ; \
samtools view -Sbh -o ${FILEPREFIX}.bam ${FILEPREFIX}.sam -@ 8 ; \
samtools sort -m 4000000000 ${FILEPREFIX}.bam ${FILEPREFIX}.sorted -@ 8; \
samtools index ${FILEPREFIX}.sorted.bam ; \
samtools flagstat ${FILEPREFIX}.sorted.bam > ${FILEPREFIX}.sorted.flagstat ; \
samtools idxstats ${FILEPREFIX}.sorted.bam > ${FILEPREFIX}.sorted.idxstats ; \
echo REFOMAT END `date` | tee -a ${LOGFILE} ; \
echo DEDUPLICATE START `date` | tee -a ${LOGFILE} ; \
picard MarkDuplicates -Xmx8g MAX_FILE_HANDLES=100 AS=true I=${FILEPREFIX}.sorted.bam O=${FILEPREFIX}.unique.bam M=${FILEPREFIX}.unique.metric.txt ; \
samtools index ${FILEPREFIX}.unique.bam ; \
samtools flagstat ${FILEPREFIX}.unique.bam | tee ${FILEPREFIX}.unique.flagstat ; \
samtools idxstats ${FILEPREFIX}.unique.bam | tee ${FILEPREFIX}.unique.idxstats ; \
echo DEDUPLICATE END `date` | tee -a ${LOGFILE}

##Run Genome mapping

FASTQFILEPREFIX=${RLIB}-${RUN} ; \
FILEPREFIX=${RLIB}-${RUN}-genome ; \
LOGFILE=${FILEPREFIX}.map.log ; \
echo `hostname` | tee ${LOGFILE} ; \
echo MAP START `date` | tee -a ${LOGFILE} ; \
module load bowtie2
module load samtools
module load picard
bowtie2 -p 16 -t --phred33 --very-sensitive-local --un-bz2 ${FILEPREFIX}.unpaired.unaligned.sam.bz2 --al-bz2 ${FILEPREFIX}.unpaired.aligned.sam.bz2 --un-conc-bz2 ${FILEPREFIX}.discordant.sam.bz2 --al-conc-bz2 ${FILEPREFIX}.concordant.sam.bz2 genome/Pvi0Genome -1 ${FASTQFILEPREFIX}.trimmed.fastq.1 -2 ${FASTQFILEPREFIX}.trimmed.fastq.2 -S ${FILEPREFIX}.sam 2>&1 | tee -a ${LOGFILE} ; \
echo MAP END `date` | tee -a ${LOGFILE} ; \
echo REFOMAT START `date` | tee -a ${LOGFILE} ; \
samtools view -Sbh -o ${FILEPREFIX}.bam ${FILEPREFIX}.sam -@ 8; \
samtools sort -m 4000000000 ${FILEPREFIX}.bam ${FILEPREFIX}.sorted -@ 8; \
samtools index ${FILEPREFIX}.sorted.bam ; \
samtools flagstat ${FILEPREFIX}.sorted.bam > ${FILEPREFIX}.sorted.flagstat ; \
samtools idxstats ${FILEPREFIX}.sorted.bam > ${FILEPREFIX}.sorted.idxstats ; \
echo REFOMAT END `date` | tee -a ${LOGFILE} ; \
echo DEDUPLICATE START `date` | tee -a ${LOGFILE} ; \
picard MarkDuplicates -Xmx8g MAX_FILE_HANDLES=100 AS=true I=${FILEPREFIX}.sorted.bam O=${FILEPREFIX}.unique.bam M=${FILEPREFIX}.unique.metric.txt ; \
samtools index ${FILEPREFIX}.unique.bam ; \
samtools flagstat ${FILEPREFIX}.unique.bam | tee ${FILEPREFIX}.unique.flagstat ; \
samtools idxstats ${FILEPREFIX}.unique.bam | tee ${FILEPREFIX}.unique.idxstats ; \
echo DEDUPLICATE END `date` | tee -a ${LOGFILE}

##Run Coverage scripts

module load bedtools
module load samtools
FILEPREFIX=${RLIB}-${RUN} ; \
LOGFILE=${FILEPREFIX}.coverage.log ; \
echo `hostname` | tee ${LOGFILE} ; \
echo COVERAGE START `date` | tee -a ${LOGFILE} ; \
samtools view -F 0x400 -b ${FILEPREFIX}.unique.bam | genomeCoverageBed -d -split -ibam stdin | perl condenseCoverage.pl > ${FILEPREFIX}.unique.gcov ; \
echo COVERAGE END `date` | tee -a ${LOGFILE} ; \
echo DEPTH START `date` | tee -a ${LOGFILE} ; \
perl summarizeCoverage.pl ${FILEPREFIX}.unique.gcov > ${FILEPREFIX}.unique.depth ; \
echo DEPTH END `date` | tee -a ${LOGFILE}
