module load bedtools
module load samtools
RLIB=$1 ; \
RUN=$2 ; \
FILEPREFIX=${RLIB}-${RUN} ; \
LOGFILE=${FILEPREFIX}.coverage.log ; \
echo `hostname` | tee ${LOGFILE} ; \
echo COVERAGE START `date` | tee -a ${LOGFILE} ; \
samtools view -F 0x400 -b ${FILEPREFIX}.unique.bam | genomeCoverageBed -d -split -ibam stdin | perl condenseCoverage.pl > ${FILEPREFIX}.unique.gcov ; \
echo COVERAGE END `date` | tee -a ${LOGFILE} ; \
echo DEPTH START `date` | tee -a ${LOGFILE} ; \
perl summarizeCoverage.pl ${FILEPREFIX}.unique.gcov > ${FILEPREFIX}.unique.depth ; \
echo DEPTH END `date` | tee -a ${LOGFILE}
