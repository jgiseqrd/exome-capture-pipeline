use strict;
my $id = '';
while(<STDIN>) {
	chomp;
	my @parts = split(/\t/);
	if ($parts[0] ne $id) {
		if ("" ne $id) {
			print "\n";
		}
		print $parts[0], "\t", $parts[2];
		$id = $parts[0];
	} else {
		print ',', $parts[2];
	}
}
if ("" ne $id) {
	print "\n";
}
exit 0;
