#!/usr/bin/perl

##Move genome output to Genome folder
system("mv *-genome* ../Genome/");

open(WF,">computeMappingStatistics_forReports.txt") || die "Cannot open file\n";
open(CLA,">../RQC/CLAField.txt") || die "Cannot open CLAField file\n";

$ls = `ls *unique.flagstat`;
@files=split(/\n/,$ls);

print WF "LibraryName,Total Filtered Reads,Mapped Reads,Properly Paired,Singletons,Duplicates,%Mapped Reads,%Properly Paired,%Singletons,%Duplicates,Average Depth,Median Depth,LibraryName,Total Filtered Reads,Mapped Reads,Properly Paired,Singletons,Duplicates,%Mapped Reads,%Properly Paired,%Singletons,%Duplicates,CaptureEfficiency,AverageDepth(Genome),FoldEnrichment\n";
foreach $file (@files)	{
	##Process to get bowtie2 alignment statistics
	($libraryname,$others)=split(/-/,$file,2);
	$totalReads=$mapped=$properlyp=$singletons=$duplicates=0;
	open(RF,$file)|| die "Cannot open file $file\n";
	while($line=<RF>)	{
		chomp($line);
		($value,$others)=split(/\+/,$line);
		$value =~ s/\s+//g;
		if($others =~ /in total/)	{
			$totalReads=$value;
		}
		elsif($others =~ /mapped \(/)	{
			$mapped=$value;
			$pMapped=sprintf("%.2f",($mapped/$totalReads)*100);	
		}
		elsif($others =~ /properly paired/)	{
			$properlyp=$value;
			$pProperlyp=sprintf("%.2f",($properlyp/$totalReads)*100);	
		}
		elsif($others =~ /singletons/)	{
			$singletons=$value;
			$pSingletons=sprintf("%.2f",($singletons/$totalReads)*100);	
		}
		elsif($others =~ /duplicates/)	{
			$duplicates=$value;
			$pDuplicates=sprintf("%.2f",($duplicates/$totalReads)*100);	
		}
	}
	print WF "$libraryname,$totalReads,$mapped,$properlyp,$singletons,$duplicates,$pMapped,$pProperlyp,$pSingletons,$pDuplicates,";
	print CLA "$libraryname\t";
	close RF;
	#if (0==1) {
	##Process .unique.depth file to get average and median counts
	$averageDepth = 0;
	$depthFile = $file;
	$depthFile =~ s/flagstat/depth/;
	open(INFILE, $depthFile) || die "Fail to open $depthFile\n$!\n";
	$flag=0;
	$totalDepth = 0;
	$totalCount = 0;
	$percent=0;
	$averageDepth = 0;
	$midValue = 0;
	@cumCount = ();
	@cumDepth=();
	$counter=0;
	while ($line=<INFILE>) {
		if ($line =~ /^Depth\s+\#Bases/) {
			$flag=1;			
		}
		elsif ($flag ==1) {
			($depth,$count,$percent) = split(/\t/,$line);
			$depth =~ s/\s+|\t//;
			$count =~ s/\s+|\t//g;
			$totalDepth += ($depth*$count);
			$totalCount += $count;
			$cumCount[$counter] = $totalCount;
			$cumDepth[$counter] = $depth;
			$counter++;
		}	
	}
	close INFILE;
	##Compute average depth
	$averageDepth = $totalDepth/$totalCount;
	##Compute median depth
	$midValue = $totalCount/2;
	#print "MidValue:$midValue\tCumCount:$#cumCount\n";
	for($i=1;$i<=$#cumCount;$i++)	{
		#print "$cumCount[$i]\n";
		$j = $i - 1;
		if ($cumCount[$i] > $midValue && $cumCount[$j] < $midValue) {
			#print "j:$cumCount[$j]\ti:$cumCount[$i]\tm:$midValue\n";
			$median = ($cumDepth[$i] + $cumDepth[$j])/2;
			#print "$median\n";
		}
	}
		
	
	printf WF "%.2f,%.2f,",$averageDepth,$median;
	printf CLA "%.2f",$averageDepth;
	print CLA "\n";
	
	$genomeFile = $file;
	$genomeFile  =~ s/\.unique/-genome\.unique/;
	$totalReads=$mappedG=$properlypG=$singletonsG=$duplicatesG=0;
	open(RF, "../Genome/$genomeFile") || die "Cannot open file\n";
	while($line=<RF>)	{
		chomp($line);
		($value,$others)=split(/\+/,$line);
		$value =~ s/\s+//g;
		if($others =~ /in total/)	{
			$totalReads=$value;
		}
		elsif($others =~ /mapped \(/)	{
			$mappedG=$value;
			$pMappedG=sprintf("%.2f",($mappedG/$totalReads)*100);	
		}
		elsif($others =~ /properly paired/)	{
			$properlypG=$value;
			$pProperlypG=sprintf("%.2f",($properlypG/$totalReads)*100);	
		}
		elsif($others =~ /singletons/)	{
			$singletonsG=$value;
			$pSingletonsG=sprintf("%.2f",($singletonsG/$totalReads)*100);	
		}
		elsif($others =~ /duplicates/)	{
			$duplicatesG=$value;
			$pDuplicatesG=sprintf("%.2f",($duplicatesG/$totalReads)*100);	
		}
	}
	$captureEfficiency = sprintf("%.2f",($pMapped/$pMappedG)*100);
	$averageGenome = sprintf("%.2f",(($totalReads*300)/1358000000));
	$foldEnrichment = sprintf("%.2f",($averageDepth/$averageGenome));
	print WF "$libraryname,$totalReads,$mappedG,$properlypG,$singletonsG,$duplicatesG,$pMappedG,$pProperlypG,$pSingletonsG,$pDuplicatesG,$captureEfficiency,$averageGenome,$foldEnrichment\n";
	close RF;
	
		
	
}
close WF;
close CLA;
system("module load R;R --no-save < Reports.R");

exit;
